const db = require('./db.js');

const getCustomer = (req,res)=>{
    let qstr = "SELECT id,customerName FROM customers ";
    return db.execute(qstr);
}

const getCustomerById = (id)=>{
    let qstr = "SELECT id,customerName FROM customers WHERE id="+id;
    return db.execute(qstr);
}

const getAccount = (accountNo)=>{
    let qstr = "SELECT id FROM account WHERE accountNo="+accountNo;
    return db.execute(qstr);
}

const getAccountBalance = (accountNo)=>{
    let qstr = "SELECT sum(credit) - sum(debit) as balance FROM transaction WHERE accountNo="+accountNo;
    return db.execute(qstr);
}

const createAccount =async (req,res)=>{
    let statusCode = 200;
    let message = "";
    let rule = [
        "customerId",
        "accountNo",
        "amount"
    ];

    let validatation = await validate(req,rule);
    
    message = 'Invalid Input';
    if(validatation.length>0)
    {
        return { statusCode:422 , message,error:validatation}
    }


	let status = false ; 
	 
    let customer = await getCustomerById(req.customerId).then(([rows])=>{
            return rows[0];
        }).catch((err)=>{
            return [];
        });

    if(typeof customer === 'undefined')
    {
    	message = 'No Customer Found';
    	return { statusCode,status , message};
    }else{
        let acExist = await getAccount(req.accountNo).then(([rows])=>{
            return rows[0];
        }).catch((err)=>{
            return [];
        });

        if(typeof acExist === 'undefined')
        {
            let qstr = "INSERT INTO account (customerId,accountNo) VALUES ("+req.customerId+","+req.accountNo+")";
            let account =  db.execute(qstr).then((res)=>{
                return true;
            }).catch((err)=>{
                return false;
            });
    
            if(account)
            {
                status = true;
                let qstr = "INSERT INTO transaction (accountNo,credit,debit) VALUES ("+req.accountNo+","+req.amount+",0)";
                let trans =  db.execute(qstr).then((res)=>{
                    return true;
                }).catch((err)=>{
                    return false;
                });
                status = trans ? true : false;
            }
            return { statusCode,status  , message : status ? 'Success' : 'Failed'};
        }else{
            message = 'Account Already Exist';
            return { statusCode,status , message};
        }
    }
}

const tranferAmount = async(req,res)=>
{
    
    let statusCode = 200;
    let message = "";
    let rule = [
        "fromAccount",
        "toAccount",
        "amount"
    ];

    let validatation = await validate(req,rule);
    
    message = 'Invalid Input';
    if(validatation.length>0)
    {
        return { statusCode:422 , message,error:validatation}
    }

    let status = false ;
	 
    let fromAccount = await getAccount(req.fromAccount).then(([rows])=>{
            return rows[0];
        }).catch((err)=>{
            return [];
        });
    let toAccount = await getAccount(req.toAccount).then(([rows])=>{
        return rows[0];
    }).catch((err)=>{
        return [];
    });

    if(typeof fromAccount === 'undefined' || typeof toAccount === 'undefined')
    {
    	message = 'Invalid Account Details';
    	return { status , message};
    }

    let balance = await getAccountBalance(req.fromAccount).then(([rows])=>{
        return rows[0];
    }).catch((err)=>{
        return [];
    });

    if(parseFloat(balance.balance) < req.amount )
    {   
        message = 'Insufficient balance';
    	return { status , message};  
    }

     qstr = "INSERT INTO transaction (accountNo,credit,debit) VALUES ("+req.fromAccount+",0,"+req.amount+")";
    let debit =  await db.execute(qstr).then((res)=>{
        return true;
    }).catch((err)=>{
        return false;
    });

     qstr = "INSERT INTO transaction (accountNo,credit,debit) VALUES ("+req.toAccount+","+req.amount+",0)";
    let credit =  await db.execute(qstr).then((res)=>{
        return true;
    }).catch((err)=>{
        return false;
    });

    if(credit & debit) { status = true;}

    return { status , message : status ? 'Success' : 'Failed'};  

}

const validate = async (req,rules)=>{

    let error = [];
    await rules.forEach(async (obj)=>{
        let required = obj in req; 

        if(!required)
        {
            let errVal = {
                "param":obj,
                "message": obj + " required "
            } 

            error.push(errVal);
        }
    });

    return error;

}

module.exports = {getCustomer,getCustomerById,createAccount,tranferAmount,validate,getAccountBalance}