const mysql = require('mysql2');

const pool = mysql.createConnection({
    host:'localhost',
    user:'root',
    database:'fakeBank',
    password:''
})

module.exports = pool.promise();