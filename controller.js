const url = require('url');
const model = require('./model');
 
const getCustomer = async (req,res)=>
{
    const reqUrl = url.parse(req.url, true);
    const query = reqUrl.query;
    let customer;
    if(typeof query.id === 'undefined')
    {
        customer = await model.getCustomer(req).then(([rows])=>{
            return rows;
        }).catch((err)=>{
            return [];
        });
    }else{
        customer = await model.getCustomerById(query.id).then(([rows])=>{
            return rows[0];
        }).catch((err)=>{
            return [];
        });
    }
    
    return  {
        statusCode : 200,
        customer 
    };
}

const getBalance = async (req,res)=>
{
    const reqUrl = url.parse(req.url, true);
    const query = reqUrl.query;
    if(typeof query.id === 'undefined' || query.id == '')
    {
        let error = [{
            "param":"Account NO",
            "message": " Account No required "
        }];
         

        return  {
            statusCode : 422,
            message : 'Invalid Input',
            error
        };
    }else{
        balance = await model.getAccountBalance(query.id).then(([rows])=>{
            return rows[0];
        }).catch((err)=>{
            return [];
        });
        return  {
            statusCode : 200,
            Balance : balance.balance
        };
    }
    
    
}


const createAccount = async (req,res)=>{

     callBackBody(req, async result => 
    {
        let response =await  model.createAccount(result);
        res.statusCode = response.statusCode ? response.statusCode : 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
    });
}

const tranferAmount = async (req,res)=>{
    callBackBody(req, async result => 
    {
        let response =await  model.tranferAmount(result);
        res.statusCode = response.statusCode ? response.statusCode : 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(response));
    });
}

const callBackBody =   (request, callback) => {
    
    let body = '';
    request.on('data', chunk => {
        body += chunk.toString();
    });
    request.on('end', () => {
        callback(JSON.parse(body));
    });
}


module.exports = { getCustomer , createAccount,tranferAmount,getBalance};