const http = require('http');
const url = require('url');

module.exports = http.createServer(async (req, res) => 
{

    var controller = require('./controller.js');
    const Url = url.parse(req.url, true);

    let response = {};
    
    switch(req.method)
    {
        case 'GET':
            switch(Url.pathname)
            {
                case '/getCustomer':
                    response = await controller.getCustomer(req, res);
                    break;
                case '/getBalance':
                    response = await controller.getBalance(req, res);
                    break;
                default:
                    console.log(Url.pathname);
                    response = {
                        statusCode : 405,
                        message : " Invalid Method"
                    };
                    break;
            }
            res.statusCode = response.statusCode;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(response));

            break;
        case 'POST':
            switch(Url.pathname)
            {
                case '/createAccount':
                    await controller.createAccount(req,res);
                    break;
                case '/transfer':
                    await controller.tranferAmount(req,res);
                    break;
                default:
                    response = {
                        statusCode : 405,
                        message : " Invalid Method"
                    };
                    res.statusCode = response.statusCode;
                    res.setHeader('Content-Type', 'application/json');
                    res.end(JSON.stringify(response));
            
                    break;
            }
            break;
        default:
            response = {
                statusCode : 405,
                message : " Invalid Method"
            };
            res.statusCode = response.statusCode;
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(response));
            break;
    }

   

});


// function collectRequestData(request, callback) {
    
//         let body = '';
//         request.on('data', chunk => {
//             body += chunk.toString();
//         });
//         request.on('end', () => {
//             callback( (body));
//         });
     
// }

